const writeFile = require('./write-file');

module.exports = function (opts) {
    const title = opts.data.name || 'iView project';
    const elsupport = !opts.data.wininfo.used?'':`
    if(typeof require === 'function'){
            try{
                let el = require('electron');
                window.ipc = el.ipcRenderer;
            }catch(err){
                window.ipc = null;
            }
        }else{
            window.ipc = null;
        }
    `;
    const file = `
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title>`+title+`</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
    <link rel="stylesheet" href="main.css">
    <script type="text/javascript" >
        ${elsupport}
        if (typeof Object.assign != 'function') {
          // Must be writable: true, enumerable: false, configurable: true
          Object.defineProperty(Object, "assign", {
            value: function assign(target, varArgs) { // .length of function is 2
              'use strict';
              if (target == null) { // TypeError if undefined or null
                throw new TypeError('Cannot convert undefined or null to object');
              }
    
              var to = Object(target);
    
              for (var index = 1; index < arguments.length; index++) {
                var nextSource = arguments[index];
    
                if (nextSource != null) { // Skip over if undefined or null
                  for (var nextKey in nextSource) {
                    // Avoid bugs when hasOwnProperty is shadowed
                    if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                      to[nextKey] = nextSource[nextKey];
                    }
                  }
                }
              }
              return to;
            },
            writable: true,
            configurable: true
          });
        }
    </script>
</head>
<body>
    <div id="app"></div>
    <script type="text/javascript" src="vendors.js"></script>
    <script type="text/javascript" src="main.js"></script>
</body>
</html>
    `;
    writeFile({
        directory: `${opts.directory}/dev`,
        fileName: 'index.html',
        data: file,
        codeType: 'html',
        success () {
            opts.success();
        },
        error () {
            opts.error();
        }
    });
};