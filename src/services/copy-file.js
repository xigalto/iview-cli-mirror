const fs = require('fs');
const path = require('path');

module.exports = function (opts) {
    var folders = path.dirname(opts.to).split('/');
    var folder = '';
    for(var i = 0;i < folders.length;i++){
        folder = path.join(folder,folders[i]);
        if( !fs.existsSync(folder) )
            fs.mkdirSync(folder);
    }
    var src = fs.createReadStream(opts.from);
    var to = fs.createWriteStream(opts.to);
    if(src && to){
        src.pipe(to);
    }
};