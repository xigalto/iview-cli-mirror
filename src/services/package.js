const writeFile = require('./write-file');

let file = {
    "name": "",
    "version": "1.0.0",
    "description": "",
    "main": "index.js",
    "scripts": {
        "init": "webpack --progress --config webpack.dev.config.js",
        "dev": "webpack-dev-server --content-base ./dev --open --inline --hot --compress --history-api-fallback --config webpack.dev.config.js",
        "dist": "webpack --progress --hide-modules --config webpack.prod.config.js",
        "lint": "eslint --fix --ext .js,.vue src"
    },
    "repository": {
        "type": "git",
        "url": ""
    },
    "author": "",
    "license": "MIT",
    "dependencies": {
        "axios": "^0.17.1",
        "iview": "^2.7.4",
        "vue": "^2.5.2",
        "vue-router": "^3.0.1"
      },
      "devDependencies": {
        "autoprefixer-loader": "^3.2.0",
        "babel": "^6.23.0",
        "babel-core": "^6.23.1",
        "babel-loader": "^7.1.2",
        "babel-plugin-transform-runtime": "^6.12.0",
        "babel-preset-es2015": "^6.9.0",
        "babel-runtime": "^6.11.6",
        "css-loader": "^0.28.7",
        "eslint": "^4.13.1",
        "eslint-plugin-html": "^4.0.1",
        "extract-text-webpack-plugin": "^3.0.2",
        "file-loader": "^1.1.5",
        "html-loader": "^0.5.1",
        "html-webpack-plugin": "^2.28.0",
        "iview-loader": "^1.0.0",
        "less": "^2.7.3",
        "less-loader": "^4.0.5",
        "style-loader": "^0.19.1",
        "url-loader": "^0.6.2",
        "vue-hot-reload-api": "^2.2.4",
        "vue-html-loader": "^1.2.3",
        "vue-loader": "^13.5.0",
        "vue-style-loader": "^3.0.3",
        "vue-template-compiler": "^2.2.1",
        "webpack": "^3.8.1",
        "webpack-dev-server": "^2.9.2",
        "webpack-merge": "^4.1.1"
      }
};

module.exports = function (opts) {
    const data = opts.data;

    if (data.name) file.name = data.name;
    if (data.version) file.version = data.version;
    if (data.desc) file.description = data.desc;
    if (data.git) file.repository.url = data.git;

    if (data.css.indexOf('less') > -1) {
        file.devDependencies['less'] = '^2.7.1';
        file.devDependencies['less-loader'] = '^2.2.3';
    }

    if (data.css.indexOf('sass') > -1) {
        file.devDependencies['node-sass'] = '^3.10.1';
        file.devDependencies['sass-loader'] = '^4.0.2';
    }

    if (data.ajax) file.dependencies['axios'] = '^0.15.3';
    if (data.i18n) file.dependencies['vue-i18n'] = '^5.0.3';
    if (data.store.indexOf('vuex') > -1)  file.dependencies['vuex'] = '^2.2.1';
    if (data.chart.indexOf('echarts') > -1) file.dependencies['echarts'] = '^3.4.0';
    if (data.eslint) {
        file.devDependencies['eslint'] = '^3.12.2';
        file.devDependencies['eslint-plugin-html'] = '^1.7.0';
    }

    if (data.funs.indexOf('cookies') > -1) file.dependencies['js-cookie'] = '^2.1.3';
    if (data.funs.indexOf('clipboard') > -1) file.dependencies['clipboard'] = '^1.5.12';
    if (data.funs.indexOf('html2canvas') > -1) file.dependencies['html2canvas'] = '^0.5.0-beta4';
    if (data.funs.indexOf('rasterizehtml') > -1) file.dependencies['rasterizehtml'] = '^1.2.4';
    
    if(data.wininfo.used){
        file.scripts['package'] = 'yarn dist && electron-builder --dir';
        file.scripts['publish'] = 'yarn dist && electron-builder';
        file.scripts['runexe'] = 'yarn dist && electron ./dist/exemain.js';
        file.scripts['devexe'] = 'yarn dist && electron ./dist/exemain.js --dev';
        file.devDependencies['electron-builder'] = '^20.26.0';
        file.devDependencies['electron'] = '^2.0.5';
        file.main = './dist/exemain.js';
        file.build = {
            productName: opts.productName,
            appId : `${opts.productName}.app.4kb.cc`,
            "directories":{
                "output":"publish"
            },
            files: [
              './dist/**/*'
            ]
        };
    }
                
        const exemainfile = `
    const {app, BrowserWindow} = require('electron')
    const ipc = require('electron').ipcMain;
    let mainWindow
    function createWindow () {
      mainWindow = new BrowserWindow({width: ${data.wininfo.sizex}, height: ${data.wininfo.sizey},title:'${data.title}}',webPreferences: {
          webSecurity: false,
          allowRunningInsecureContent:true  // allow http in https pages
        }})
      
      mainWindow.setMenu(null);
      mainWindow.loadURL('file://'+__dirname+'/index.html')
      if(process.argv.indexOf("--debug")>=0||process.argv.indexOf("--dev")>=0){
          mainWindow.maximize();
          mainWindow.webContents.openDevTools()
      }

      // Emitted when the window is closed.
      mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
      })
    }
    // ignore ssl error
    app.commandLine.appendSwitch('ignore-certificate-errors');
    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    // Some APIs can only be used after this event occurs.
    app.on('ready', createWindow)

    // Quit when all windows are closed.
    app.on('window-all-closed', function () {
      // On OS X it is common for applications and their menu bar
      // to stay active until the user quits explicitly with Cmd + Q
      if (process.platform !== 'darwin') {
        app.quit()
      }
    })

    app.on('activate', function () {
      // On OS X it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (mainWindow === null) {
        createWindow()
      }
    })

    // In this file you can include the rest of your app's specific main process
    // code. You can also put them in separate files and require them here.
     `;
     
    writeFile({
        directory: path.join(opts.directory,'./dev'),
        fileName: 'exemain.js',
        data: exemainfile,
        success () {
            opts.success();
        },
        error () {
            opts.error();
        }
    });

    writeFile({
        directory: opts.directory,
        fileName: 'package.json',
        data: JSON.stringify(file),
        codeFormat: {
            indent_size: 2
        },
        success () {
            opts.success();
        },
        error () {
            opts.error();
        }
    });
};